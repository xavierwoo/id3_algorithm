require 'tree'
require 'set'
include Math

$attri = ["outlook", "temperature", "humidity", "windy", "play"]
$data = [
  [:sunny,    85, 85, false, false],
  [:sunny,    80, 90, true,  false],
  [:overcast, 83, 78, false, true ],
  [:rain,     70, 96, false, true ],
  [:rain,     68, 80, false, true ],
  [:rain,     65, 70, true,  false],
  [:overcast, 64, 65, true,  true ],
  [:sunny,    72, 95, false, false],
  [:sunny,    69, 70, false, true ],
  [:rain,     75, 80, false, true ],
  [:sunny,    75, 70, true,  true ],
  [:overcast, 72, 90, true,  true ],
  [:overcast, 81, 75, false, true ],
  [:rain,     71, 80, true,  true ],
]

###########################################
#    Get the entropy of a collection
###########################################
def Entropy(collection)
	setOfC = collection.to_set
	entropy = 0
	for e in setOfC
		p = collection.count(e).to_f / collection.size.to_f
		
		entropy = entropy - p * log(p, 2)
	end
	return entropy
end

###########################################
#   Get the gain value
###########################################
def Gain(collection, collectionA)
	gain = Entropy(collection)
	setOfCA = collectionA.to_set
	
	for e in setOfCA
		collectionV = []
		for i in 0..collectionA.size
			if collectionA[i] == e 
				collectionV.push(collection[i])
			end
		end
		
		gain = gain - (collectionV.size.to_f/collection.size.to_f) * Entropy(collectionV)
	end
	
	return gain
end

##################################################
#    If the attributes are continuous ones 
#         we need divide them into two part
##################################################
def getPartition(collection, collectionV)	
	c = collectionV.zip(collection)
	
	bestPos = 0
	bestGain = 0;
	newCollection = c.collect{|x| x[1]}
	
	for pos in 0..c.size-1
		newCollectionV = c.collect{|x| x[0] <= c[pos][0] ? 1 : 2}
	
		gain = Gain(newCollection, newCollectionV)
		
		if gain > bestGain
			bestPos = pos
			bestGain = gain
		end
	end
	
	return c[bestPos][0]
end


#####################################################
#  Find the attributes with the largest gain value
#####################################################
def getLargestGain(data, attri)

	collection = data.collect{|x| x[4]}
	
	gains = Array.new(4){|x| nil}
	
	for e in attri
		if e == 0 || e == 3
			collectionV = data.collect{|x| x[e]}
			gains[e] = Gain(collection, collectionV), nil
		elsif e == 1 || e == 2
			collectionV = data.collect{|x| x[e]}
			parPoint = getPartition(collection, collectionV)
			collectionV = data.collect{|x| x[e] <= parPoint ? 1 : 2}
			gains[e] = Gain(collection, collectionV), parPoint
		end
	end
	
	return gains
end


#######################################################
#      The main body of the id3 algorithm
#######################################################
def ID3(treeNode, nonCatAttr, catAttr, data)
	
	#If there is no data left...
	if data == []
		treeNode<<Tree::TreeNode.new("Lack of data", "End")
		return
	end
	
	
	#If the left data are records all with the same value
	if data.collect{|x|x[4]}.to_set.size <= 1
		treeNode<<Tree::TreeNode.new(data[0][4], "End")
		return
	end
	
	#If there are no non-categorical attributes
	if nonCatAttr == []
	
		d = data.collect{|x| x[4]}
		
		guessed = d.count(true) > d.count(false) ? "true" : "false"
		
		treeNode<<Tree::TreeNode.new(guessed + " guess", "End")
		return
	end
	
	
	#Calculate gain values for all the attributes
	gains = getLargestGain(data, nonCatAttr)
	
	
	#get the largest
	iLargest = nil
	for i in 0..gains.size-1
		if gains[i] != nil
			if iLargest == nil
				iLargest = i
			elsif gains[i][0] > gains[iLargest][0] 
				iLargest = i
			end
		end
	end
	

	nonCatAttr = nonCatAttr - [iLargest]
	catAttr = catAttr + [iLargest]
	
	collection = data.collect{|x| x[iLargest]}
	
	#If the largest attribute is discrete
	if gains[iLargest][1] == nil
		
		s = collection.to_set
		
		for e in s
			d = data.select{|x| x[iLargest] == e}
			
			tN = Tree::TreeNode.new($attri[iLargest] +'`' + e.to_s, e)
			treeNode<<tN
			ID3(tN, nonCatAttr, catAttr, d)
		end
		
	#If the largest attribute is continuous
	else
		parPoint = gains[iLargest][1]
		d = data.select{|x| x[iLargest] <= parPoint}
		tN = Tree::TreeNode.new($attri[iLargest] +"  <="+parPoint.to_s)
		treeNode<<tN
		ID3(tN, nonCatAttr, catAttr, d)
		
		
		d = data.select{|x| x[iLargest] > parPoint}
		
		tN = Tree::TreeNode.new($attri[iLargest] +"  >"+parPoint.to_s)
		treeNode<<tN
		ID3(tN, nonCatAttr, catAttr, d)

	end
	
	
end

def buildTree()
	
	nAttri = [0,1,2,3]
	cAttri = []
	d = $data
	tree = Tree::TreeNode.new("Root", "root")
	ID3(tree, nAttri, cAttri, d)
	
	tree.print_tree
end



#### The Main function ####
if __FILE__ == $0
	
	
	buildTree()
	
	
end




















